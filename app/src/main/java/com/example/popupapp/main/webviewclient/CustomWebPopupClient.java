package com.example.popupapp.main.webviewclient;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.popupApp.databinding.ActivityMainBinding;
import com.example.popupApp.databinding.ActivityPopupBinding;

import java.net.URISyntaxException;

public class CustomWebPopupClient extends WebViewClient {
    ActivityPopupBinding binding;
    public CustomWebPopupClient(ActivityPopupBinding activityMainBinding) {
        this.binding = activityMainBinding;
    }

    public void onPageFinished(WebView webview, String url) {
        System.out.println("Page loaded");
        super.onPageFinished(webview, url);
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        // Решите, следует ли продолжать загрузку страницы или прервать в случае ошибки SSL
        // handler.proceed(); // продолжить загрузку страницы
        // handler.cancel(); // прервать загрузку страницы
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith("http") || url.startsWith("https")) {
            return false;
        }
        if (url.startsWith("intent")) {
            try {
                Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                String fallbackUrl = intent.getStringExtra("browser_fallback_url");
                if (fallbackUrl != null) {
                    binding.popupWebView.loadUrl(fallbackUrl);
                    return true;
                }
            } catch (URISyntaxException e) {

            }
        }
        view.loadUrl(url);
        return true;
    }
    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        // Обработайте ошибки здесь
        super.onReceivedError(view, request, error);
    }

    @TargetApi(Build.VERSION_CODES.N)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        view.loadUrl(request.getUrl().toString());
        return true;
    }
}
