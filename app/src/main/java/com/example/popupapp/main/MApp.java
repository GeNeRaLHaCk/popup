package com.example.popupapp.main;

import android.app.Application;

import com.google.firebase.FirebaseApp;

public class MApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize Firebase
        FirebaseApp.initializeApp(this);
    }
}
