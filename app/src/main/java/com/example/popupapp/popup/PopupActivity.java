package com.example.popupapp.popup;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.popupApp.R;
import com.example.popupApp.databinding.ActivityPopupBinding;

public class PopupActivity extends Activity {
    private WebView popupWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);

        popupWebView = findViewById(R.id.popupWebView);
        WebSettings webSettings = popupWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        popupWebView.setWebViewClient(new WebViewClient());

        String url = getIntent().getStringExtra("url");
        popupWebView.loadUrl(url);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 5000);
    }

}
